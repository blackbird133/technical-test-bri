fun highestPalindrome(s: String, k: Int): String {
    val chars = s.toCharArray()
    val n = chars.size
    val changed = BooleanArray(n)

    fun makePalindrome(left: Int, right: Int, changes: Int): Int {
        if (left >= right) return changes
        if (chars[left] != chars[right]) {
            val maxChar = maxOf(chars[left], chars[right])
            chars[left] = maxChar
            chars[right] = maxChar
            changed[left] = true
            changed[right] = true
            return makePalindrome(left + 1, right - 1, changes + 1)
        }
        return makePalindrome(left + 1, right - 1, changes)
    }

    val changesNeeded = makePalindrome(0, n - 1, 0)
    if (changesNeeded > k) return "-1"

    var remainingChanges = k - changesNeeded

    fun maximizePalindrome(left: Int, right: Int) {
        if (left >= right || remainingChanges <= 0) return

        if (left < n / 2 && chars[left] != '9' && (changed[left] || remainingChanges >= 2)) {
            chars[left] = '9'
            chars[right] = '9'
            remainingChanges -= if (changed[left]) 1 else 2
        } else if (right >= n / 2 && chars[right] != '9' && (changed[right] || remainingChanges >= 2)) {
            chars[left] = '9'
            chars[right] = '9'
            remainingChanges -= if (changed[right]) 1 else 2
        }

        maximizePalindrome(left + 1, right - 1)
    }

    maximizePalindrome(0, n - 1)

    return String(chars)
}

fun main() {
    val stringInput1 = "3923"
    val k1 = 1

    val stringInput2 = "93987239"
    val k2 = 1

    val stringInput3 = "abbcdxa"
    val k3 = 2

    println("Test Case1:\nInputed String = $stringInput1, K1 = $k1\nResult: ${highestPalindrome(stringInput1, k1)}" )
    println("\nTest Case2:\nInputed String = $stringInput2, K2 = $k2\nResult: ${highestPalindrome(stringInput2, k2)}" )
    println("\nTest Case3:\nInputed String = $stringInput3, k3 = $k3\nResult: ${highestPalindrome(stringInput3, k3)}")
}