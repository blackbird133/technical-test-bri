fun isBalanced(s: String): String {
    val stack = mutableListOf<Char>()
    val matchingBrackets = mapOf(')' to '(', ']' to '[', '}' to '{')  // Fix the map

    for (char in s) {
        when (char) {
            '(', '[', '{' -> stack.add(char)
            ')', ']', '}' -> {
                if (stack.isEmpty() || stack.removeAt(stack.size - 1) != matchingBrackets[char]) {
                    return "NO"
                }
            }
        }
    }
    return if (stack.isEmpty()) "YES" else "NO"
}

fun main() {
    val stringInput1 = "(){}[]()"
    val stringInput2 = "{ [ ([] )}".replace("\\s".toRegex(), "")
    val stringInput3 = "{ ( ( [ ] ) [ ] ) [ ] }".replace("\\s".toRegex(), "")

    println("Test Case1:\nInputed String = $stringInput1\nResult: ${isBalanced(stringInput1)}")
    println("\nTest Case2:\nInputed String = $stringInput2\nResult: ${isBalanced(stringInput2)}")
    println("\nTest Case3:\nInputed String = $stringInput3\nResult: ${isBalanced(stringInput3)}")

}