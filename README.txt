Time Complexity:
- Untuk fungsi isBalanced(), terdapat iterasi String, fungsi iterasi melalui setiap karakter dalam string input 's'. Maka Time Complexity untuk fungsi tersebut adalah O(n), dimana n adalah size dari string tersebut.
- Untuk operasi stack (pop and push) membutuhkan Waktu O(1), karena setiap karakter hanya bisa di push dan pop sekali dari setiap stack, maka dari itu Waktu total untuk operasi stack juga O(n).
Maka dari itu, untuk Time Complexity nya  adalah O(n).

Space Complexity
- Ketika penginilisasian stack dan map untuk mengetahuin MatchedBracket, membutuhkan Waktu konstan O(1)
- Iterasi melalui String, for loop melakukan iterasi setiap karakter dalam string, maka dari itu kompleksitasnya adalah O(n)
- Untuk pengecekan akhir untuk menentukan "YES" or "NO" merupakan operasi konstan O(1)
Maka dari itu, untuk Space Complexity nya adalah O(n)

Kesimpulan:
- Time Complexity: O(n)
- Space Complexity: O(n)