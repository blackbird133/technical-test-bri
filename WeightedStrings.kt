val charWeightMap = mapOf(
    'a' to 1, 'b' to 2, 'c' to 3, 'd' to 4, 'e' to 5, 'f' to 6, 'g' to 7,
    'h' to 8, 'i' to 9, 'j' to 10, 'k' to 11, 'l' to 12, 'm' to 13, 'n' to 14,
    'o' to 15, 'p' to 16, 'q' to 17, 'r' to 18, 's' to 19, 't' to 20, 'u' to 21,
    'v' to 22, 'w' to 23, 'x' to 24, 'y' to 25, 'z' to 26
)

fun getCharWeight(c: Char): Int {
    return charWeightMap[c] ?: 0
}

fun getSubstringsWeightMap(s: String): Set<Int> {
    val sortedString = s.toCharArray().sorted().joinToString("")
    val weights = mutableSetOf<Int>()
    var i = 0
    while (i < sortedString.length) {
        val currentChar = sortedString[i]
        var weight = 0
        var count = 0
        while (i < sortedString.length && sortedString[i] == currentChar) {
            count++
            weight += getCharWeight(currentChar)
            weights.add(weight)
            i++
        }
    }
    return weights
}

fun weightedStrings(s: String, queries: List<Int>): List<String> {
    val weights = getSubstringsWeightMap(s)
    return queries.map { query -> if (weights.contains(query)) "Yes" else "No" }
}

fun main() {
    val stringInput1 = "abbcccd"
    val queriesInput1 = listOf(1, 3, 9, 8)
    val stringInput2 = "zzzyyybbaccddd"
    val queriesInput2 = listOf(1, 26, 52, 50, 6, 4, 25, 2, 3, 10)
    val stringInput3 = "abzdbccdcd"
    val queriesInput3 = listOf(1, 3, 9, 8, 26, 6, 12 )
    val result1 = weightedStrings(stringInput1, queriesInput1)
    val result2 = weightedStrings(stringInput2, queriesInput2)
    val result3 = weightedStrings(stringInput3, queriesInput3)
    println("Test Case1:\nInputed String = $stringInput1, Queries = $queriesInput1\nResult: $result1" )
    println("\nTest Case2:\nInputed String = $stringInput2, Queries = $queriesInput2\nResult: $result2" )
    println("\nTest Case3:\nInputed String = $stringInput3, Queries = $queriesInput3\nResult: $result3")
}